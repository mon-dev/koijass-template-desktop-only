$(document).ready(function () {
    $('.slider').slick({
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><img src="./images/arrow-left.jpg"></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="./images/arrow-right.jpg"></button>'
    });

    $('.all button').click(function () {
        $('.all button').removeClass('active');
        $(this).addClass('active');
    })

    $('.portofolio-slider').slick({
        dots: true,
        arrows: false
    });

});